﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;


public class PoolManager : MonoBehaviour
{
    private static PoolManager _instance;
    public static PoolManager Instance
    {
        get
        {
            if (_instance == null)
            {
                GameObject go = new GameObject("PoolManager");
                go.AddComponent<PoolManager>();
            }
            return _instance;
        }
    }
    public GameObject bulletFab;
    public int bulletsToSpawn = 30;
    public GameObject bulleyContainer;
    public List<GameObject> bulletsList = new List<GameObject>();

    private void Awake()
    {
        _instance = this;
    }

    private void Start()
    {
        for (int i = 0; i < bulletsToSpawn; i++)
        {
            transform.position = new Vector3(10, 800, 30);
            GameObject bullet = Instantiate(bulletFab, transform.position, Quaternion.identity) as GameObject;
            bullet.transform.parent = bulleyContainer.transform;
            bullet.SetActive(false);
            bulletsList.Add(bullet);
        }
    }
    //Какие обьекты create  instantiate 
    //public GameObject bulletHolePrefab;
    ////где и как будем хранитьlist обьеутов
    //public List<GameObject> bulletHoleList;
}
