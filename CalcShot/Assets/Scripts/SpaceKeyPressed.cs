﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SpaceKeyPressed : MonoBehaviour
{
    // Start is called before the first frame update
    void Start()
    {
        
    }

    // Update is called once per frame
    void Update()
    {
        if (Input.GetKeyDown(KeyCode.Space))
        {
            for (int i = 0; i < PoolManager.Instance.bulletsList.Count; i++)
            {
                if (PoolManager.Instance.bulletsList[i].activeInHierarchy==false)
                {
                    PoolManager.Instance.bulletsList[i].SetActive(true);
                    PoolManager.Instance.bulletsList[i].transform.position = Vector3.zero;
                    break;

                }

            }

        }
        
    }
}
